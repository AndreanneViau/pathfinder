class WayPoint{
  
  private int i;
  private int j;
  
  WayPoint(){
    i = 0;
    j = 0;
  }
  
  WayPoint(int ni, int nj){
    i = ni;
    j = nj;
  }
  
  int getI(){return i;}
  int getJ(){return j;}
  
  void setI(int ni){i = ni;}
  void setJ(int nj){j = nj;}
  
}
