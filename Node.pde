class Node extends Cell {

  // Variables pour le pathfinding
  private int G;
  private int H;
  private int movementCost;

  Node parent; // Cellule parent


  boolean isStart = false; // Definit si la cellule est une la case départ
  boolean isEnd = false; // Definit si la cellule est une la case fin
  boolean selected = false; // Definit si la cellule est sélectionnée pour le chemin

  boolean isWalkable = true; // Definit si la cellule est marchable  

  Node (int _x, int _y) {
    super (_x, _y);

    G = 9999999;
    H = 0;
  }

  Node (int _x, int _y, int _w, int _h) {
    super (_x, _y, _w, _h);

    G = 9999999;
    H = 0;
    movementCost = 10;
  }


  boolean isWalkable() {
    return isWalkable;
  }


  int getF() {
    return G + H;
  }

  int getH() {
    return H;
  }

  int getG() {
    return G;
  }

  int getMovementCost() {
    return movementCost;
  }

  void setH (int h) {
    this.H = h;
  }
  void setG(int g) {
    this.G = g;
  }




  void setMovementCost (int cost) {
    movementCost = cost;


    if (this.parent != null) {
      G = movementCost + parent.getF();
    }
  }

  Node getParent () {
    return this.parent;
  }    

  void setParent (Node parent) {
    this.parent = parent;

    if (this.parent != null) {
      this.G = this.movementCost + parent.getF();
    }
  }

  String toString() {
    return "( " + getF() + ", " + getG() + ", " + getH() + ")";
  }



  //   **************   wow !!!  **********************************************************  
  //void setWalkable (boolean value, int bgColor) {
  //  isWalkable =false;
  //  fillColor = bgColor;
  //}
  //   **************   wow !!!  **********************************************************




  void setWalkable (boolean value, int bgColor) {
    if (!(isStart || isEnd)) {
      isWalkable =false;
      fillColor = bgColor;
    }
  }




  void display () {
    pushMatrix();
    translate (x, y);
    fill (fillColor);
    stroke (0);
    rect (0, 0, w, h);
    popMatrix();
  }
}
