/********************
  Énumération des directions
  possibles
  
********************/
enum Direction {
  EAST, SOUTH, WEST, NORTH
}

/********************
  Représente une carte de cellule permettant
  de trouver le chemin le plus court entre deux cellules
  
********************/
class NodeMap extends Matrix {
  Node start;
  Node end;
  
  ArrayList <Node> path;
  
  boolean debug = false;
  
  NodeMap (int nbRows, int nbColumns) {
    super (nbRows, nbColumns);
  }
  
  NodeMap (int nbRows, int nbColumns, int bpp, int width, int height) {
    super (nbRows, nbColumns, bpp, width, height);
  }
  
  void init() {
    
    cells = new ArrayList<ArrayList<Cell>>();
    
    for (int j = 0; j < rows; j++){
      // Instanciation des rangees
      cells.add (new ArrayList<Cell>());
      
      for (int i = 0; i < cols; i++) {
        Cell temp = new Node(i * cellWidth, j * cellHeight, cellWidth, cellHeight);
        
        // Position matricielle
        temp.i = i;
        temp.j = j;
        
        cells.get(j).add (temp);
      }
    }
    
    println ("rows : " + rows + " -- cols : " + cols);
  }
  
  /*
    Configure la cellule de départ
  */
  void setStartCell (int i, int j) {
    
    if (start != null) {
      start.isStart = false;
      start.setFillColor(baseColor);
    }
    
    
    start = (Node)cells.get(j).get(i);
    start.isStart = true;
    start.isWalkable = true;
    
    if(start.isEnd){
      start.isEnd = false;
      end = null;
    }
    
    start.setFillColor(color (0, 0, 255));
  }
  
  /*
    Configure la cellule de fin
  */
  void setEndCell (int i, int j) {
    
    if (end != null) {
      end.isEnd = false;
      end.setFillColor(baseColor);
    }
    
    end = (Node)cells.get(j).get(i);
    end.isEnd = true;
    end.isWalkable = true;
    
    if(end.isStart){
      end.isStart = false;
      start = null;
    }
    
    end.setFillColor(color (255, 255, 0));
  }
  
  /** Met a jour les H des cellules
  doit etre appele apres le changement du noeud
  de debut ou fin
  */
  void updateHs() {
    for (int j = 0; j < rows; j++) {
      for (int i = 0; i < cols; i++) {
        Node current = (Node)cells.get(j).get(i); 
        current.setH( calculateH(current, end));
        current.setParent(null);
        //println(current.getH());
      }
    }
  }
  
  // Permet de generer aleatoirement le cout de deplacement
  // entre chaque cellule
  void randomizeMovementCost() {
    for (int j = 0; j < rows; j++) {
      for (int i = 0; i < cols; i++) {
        
        int cost = parseInt(random (0, cols)) + 1;
        
        Node current = (Node)cells.get(j).get(i);
        current.setMovementCost(cost);
       
      }
    }
  }
  
  // Permet de generer les voisins de la cellule a la position indiquee
  
  
  /**
    Génère les voisins de chaque Noeud
  */
  
  /*
    Permet de trouver le chemin le plus court entre
    deux cellules
  */
  void findAStarPath () {
    // TODO : Complétez ce code
    
    
    if (start == null || end == null) {
      println ("No start and/or no end defined!");
      return;
    }
    if(start.i == end.i && start.j == end.j){
      println("Start is end");
      return;
    }
    
    println("Start:", start.i, start.j, "  End:", end.i, end.j);
    
    ArrayList<Cell> closedList = new ArrayList<Cell>();
    ArrayList<Cell> openList = new ArrayList<Cell>();
    
    Cell currentNode = null;
    
    
    boolean foundPath = false;
    
    for(int i = 0; i < cells.size(); i++){
      
      ArrayList<Cell> tempPart = cells.get(i);
      
      for(int j = 0; j < tempPart.size(); j++){
        
        Node part = (Node)tempPart.get(j);
        
        part.setH(calculateH(part, end));
        
      }
      
    }
    
    start.setG(0);
    openList.add(start);
    
    while(openList.size() > 0){
      
      
      currentNode = getLowestCost(openList);
      if(currentNode != end){
        
        openList.remove(currentNode);
        closedList.add(currentNode);
        
        boolean foundParent = false;
        
        for(int i = -1; i <= 1; i++){
          for(int j = -1; j <= 1; j++){
            
            if(!(i == 0 && j == 0) && !foundParent){
              
              if(currentNode.j + j >= 0 && currentNode.j + j < cells.size()){
                
                ArrayList<Cell> tempPart = cells.get(currentNode.j + j);
                
                if(currentNode.i + i >= 0 && currentNode.i + i < tempPart.size()){
                  
                  Node part = (Node)tempPart.get(currentNode.i + i);
                  
                  if(!closedList.contains(part)){
                    
                    if(part.isWalkable()){
                      
                      int gPrime = calculateCost((Node)currentNode, part);
                      
                      if(!openList.contains(part)){
                        
                        //part.setG(gPrime);
                        part.setParent((Node)currentNode);
                        openList.add(part);
                        
                      }else {
                        
                        if(gPrime < part.getG()){
                          part.setParent((Node)currentNode);
                          part.setG(gPrime);
                          foundParent = true;
                        }
                        
                      }
                      
                    }//is walkable
                    
                  }//not in closed list
                  
                }//index  bounds y
                
              }//index  bounds x
              
            }//not center index
            
          }
        }
        
      }else {
        
        generatePath((Node)currentNode);
        foundPath = true;
        openList.clear();
        
      }
      
    }
    
    
    if(!foundPath){
      println("Stuck in maze");
    }else {
      println("Hurray!");
    }
    
  }
  
  /*
    Permet de générer le chemin une fois trouvée
  */
  void generatePath (Node endNode) {
    // TODO : Complétez ce code
    
    
    boolean hasNeighbour = true;
    path = new ArrayList<Node>();
    
    Node part = endNode;
    
    while(hasNeighbour){
      
      if(part.getParent() != null){
        
        if(!(part.isEnd || part.isStart)){
          if(points != null){
            colorMode(HSB);
            part.setFillColor(color(itteration * 255 / points.size(), 255, 255));
            colorMode(RGB);
          }else {
            part.setFillColor(color(255, 0, 0));
          }
        }
        path.add(part);
        part = part.getParent();
        
      }else {
        
        hasNeighbour = false;
        
      }
      
    }
    
    
    
  }
  
  /**
  * Cherche et retourne le noeud le moins couteux de la liste ouverte
  * @return Node le moins couteux de la liste ouverte
  */
  private Node getLowestCost(ArrayList<Cell> openList) {
    // TODO : Complétez ce code
    Node bestNode = null;
    
    for(int i = 0; i < openList.size(); i++){
      
      Node part = (Node)openList.get(i);
      
      if(bestNode != null){
        
        if(part.getF() < bestNode.getF()){
          bestNode = part;
        }
        
      }else {
        bestNode = part;
      }
      
    }
    
    return bestNode;
  }
  

  
 /**
  * Calcule le coût de déplacement entre deux noeuds
  * @param nodeA Premier noeud
  * @param nodeB Second noeud
  * @return
  */
  private int calculateCost(Node nodeA, Node nodeB) {
    // TODO : Complétez ce code
    return nodeA.getF() + nodeB.getMovementCost();
  }
  
  /**
  * Calcule l'heuristique entre le noeud donnée et le noeud finale
  * @param node Noeud que l'on calcule le H
  * @return la valeur H
  */
  private int calculateH(Cell node, Cell goal) {
    // TODO : Complétez ce code
    return abs(node.i - goal.i) + abs(node.j - goal.j);
  }
  
  String toStringFGH() {
    String result = "";
    
    for (int j = 0; j < rows; j++) {
      for (int i = 0; i < cols; i++) {

        Node current = (Node)cells.get(j).get(i);
        
        result += "(" + current.getF() + ", " + current.getG() + ", " + current.getH() + ") ";
      }
      
      result += "\n";
    }
    
    return result;
  }
  
  // Permet de créer un mur à la position _i, _j avec une longueur
  // et orientation données.
  void makeWall (int _i, int _j, int _length, boolean _vertical) {
    int max;
    
    if (_vertical) {
      max = _j + _length > rows ? rows: _j + _length;  
      
      for (int j = _j; j < max; j++) {
        ((Node)cells.get(j).get(_i)).setWalkable (false, color(255));
      }       
    } else {
      max = _i + _length > cols ? cols: _i + _length;  
      
      for (int i = _i; i < max; i++) {
        ((Node)cells.get(_j).get(i)).setWalkable (false, color(255));
      }     
    }
  }
}
