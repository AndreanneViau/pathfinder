NodeMap worldMap; //<>//

int deltaTime = 0;
int previousTime = 0;

int mapRows = 1;
int mapCols = 1;

color baseColor = color (0, 127, 0);

ArrayList<WayPoint> points;


int itteration;

void setup () {
  size (600, 600);
  //fullScreen();
  

  initMapRandom();
}

void draw () {
  deltaTime = millis () - previousTime;
  previousTime = millis();

  update(deltaTime);
  display();
}

void update (float delta) {
}

void display () {
  background(0);
  if (worldMap.path != null) {
    //for (Cell c : worldMap.path) {
    //  c.setFillColor(color (127, 0, 0));
    //}
  }

  worldMap.display();
}




void initPoints(){
  
  mapRows = 1;
  mapCols = 1;
  
  points = new ArrayList<WayPoint>();
  
  //NUMERO ETUDIANT HARDOCDED
  //9171576
  
  
  addWayPoint(91, 66);
  addWayPoint(71, 57);
  addWayPoint(57, 76);
  addWayPoint(66, 91);
  
}

void addWayPoint(int i, int j){
  points.add(new WayPoint(i, j));
  if(i > mapCols){
    mapCols = i + 1;
  }
  if(j > mapRows){
    mapRows = j + 1;
  }
}

void initMapRandom () {
  
  mapRows = 100;
  mapCols = 100;
  
  worldMap = new NodeMap (mapRows, mapCols); 

  worldMap.setBaseColor(baseColor);


  int wallSpacing = 3;

  worldMap.makeWall (mapCols / 2, wallSpacing, mapRows - wallSpacing*2, true);
  worldMap.makeWall (wallSpacing, mapRows / 2, mapCols - wallSpacing*2, false);


  // Départ fixe
  //worldMap.setStartCell(mapCols / 2 - 5, 3);
  //worldMap.setEndCell(mapCols - 1, mapRows - 1);
  
  itteration = 0;
  
  // Départ aléatoire
  int startX = int(random(mapCols));
  int startY = int(random(mapRows));

  int endX = int(random(mapCols));
  int endY = int(random(mapRows));

  while (endX == startX) {
    endX = int(random(mapCols));
  }
  while (endY == startY) {
    endY = int(random(mapRows));
  }
  
  worldMap.setStartCell(startX, startY);
  worldMap.setEndCell(endX, endY);
  //  fin Départ aléatoire



  worldMap.updateHs();
  
  worldMap.findAStarPath();
 
}


void initMapWayPoints(){
  
  initPoints();
  
  worldMap = new NodeMap (mapRows, mapCols); 

  worldMap.setBaseColor(baseColor);

  int wallSpacing = 3;

  worldMap.makeWall (mapCols - mapCols / 6, wallSpacing, mapRows - wallSpacing*2, true);
  worldMap.makeWall (wallSpacing, mapRows - mapRows / 6, mapCols - wallSpacing*2, false);


  // Hardcoded start and end
  //worldMap.setStartCell(mapCols / 2 - 5, 3);
  //worldMap.setEndCell(mapCols - 1, mapRows - 1);
  
  for(int i = 0; i < points.size(); i++){
    
    itteration = i;
    
    WayPoint part = points.get(i);
    WayPoint opart = null;
    if(i+1 < points.size()){
      opart = points.get(i+1);
    }else {
      opart = points.get(0);
    }
    
    println(part.getI(), part.getJ(), opart.getI(), opart.getJ());
    
    worldMap.setStartCell(part.getI(), part.getJ());
    worldMap.setEndCell(opart.getI(), opart.getJ());
    
    worldMap.updateHs();
    
    worldMap.findAStarPath();
    
    
  }
  
  
}



void keyPressed(){
  if (key =='r'){
    initMapRandom();
  }else if(key == 'w'){
    initMapWayPoints();
  }
}
